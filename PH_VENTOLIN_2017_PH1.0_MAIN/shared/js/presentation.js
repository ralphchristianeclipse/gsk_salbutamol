/**********************************************************/
/* GSK Veeva Master Template - Presentation Functionality */
/**********************************************************/
/* File version              1.4.1                       */
/* Last modified             05/05/2017                   */
/* Last modified by          T.T.                         */
/**********************************************************/

// --- CUSTOM TO THIS PRESENTATION --- //
$(document).ready(function() {
  isIntro =
    com.gsk.mt.currentSlide === "PH_VENTOLIN_2017_PH1.0_000" ||
    com.gsk.mt.currentSlide === "PH_VENTOLIN_2017_PH1.0_001";

  audio = new Audio();
  audio.src = isIntro
    ? "../shared/media/content/intro.mp3"
    : "../shared/media/content/game.mp3";
  audio.loop = true;

  sessionStorage[isIntro ? "gameAudioTime" : "introAudioTime"] = 0;
  audio.currentTime =
    sessionStorage[isIntro ? "introAudioTime" : "gameAudioTime"] || 0;
  audio.autoplay = true;

  window.onunload = function() {
    sessionStorage[isIntro ? "introAudioTime" : "gameAudioTime"] =
      audio.currentTime;
  };
});
